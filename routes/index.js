const express = require('express');
const router = express.Router();

// Do work here
router.get('/', (req, res) => {
    // res.send('Hey! It works!');
    const obj = { name: 'Jeremy', age: 100, fart: true };
    // res.json(obj);
    // res.send(req.query.name);
    // res.json(req.query);
    res.render('hello', {
        name: 'Jeremy',
        cat: req.query.cat
    });
});

router.get('/reverse/:name', (req, res) => {
    const reverse = [...req.params.name].reverse().join('');
    res.send(reverse);
});

module.exports = router;
